library(tidyverse)
library(rtweet)
library(lubridate)

# First store tokens
#  create_token(
#   app = app_name,
#   consumer_key = consumer_key,
#   consumer_secret = consumer_secret,
#   access_token = access_token,
#   access_secret = access_secret)

logfile <- file.path("~","Seuremediabotti", "twiits", "logs.csv") 

logs <-  read_csv(logfile) 

firstNaIndex <- logs$twiited %>% 
  is.na() %>% 
  which() %>% 
  min()

if (firstNaIndex != Inf){
  x <-  logs %>% 
    slice(firstNaIndex) %>% 
    summarise(x = paste0(text, ": ",  link, " #hyvässäseuressa")) %>% 
    pull(x)
  
  Encoding(x) <- "ISO-8859-1"
  
  post_tweet(x)
  
  #save tweet to logs
  logs$twiited[firstNaIndex] <- TRUE
  write_csv(logs, logfile)
  
}

